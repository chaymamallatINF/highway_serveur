package tn.esprit.pidev.service;


import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.entity.StockOption;
import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.entity.Stock;

@Remote
public interface StockRemoteService {

	
	public  int ajouterStock(Stock stock);
	public void deleteStockByid(int StockId);
	public Stock getStockById(int StockId);
	public void UpdateStock(int StockId,int nbr_total_of_shares,float total_value);
	public List<Stock> getStock(); 
	
}
