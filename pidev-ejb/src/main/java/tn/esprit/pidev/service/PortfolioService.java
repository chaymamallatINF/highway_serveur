package tn.esprit.pidev.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;


import tn.esprit.pidev.entity.CurrencyOption;
import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.entity.StockOption;
import tn.esprit.pidev.entity.Trade;
import tn.esprit.pidev.entity.User;
//import tn.esprit.pidev.entity.Trader;
@LocalBean
@Stateless
public class PortfolioService implements PortfolioInterface{
	

		@PersistenceContext
		EntityManager entityManager;
		//Logger logger=Logger.getLogger("Portfolio_SERVICE");
		
		@Override
		public void addPortfolio(Portfolio portfolio) {
			entityManager.persist(portfolio);
			//logger.info("portfolio recu");
		}

		

		@Override
		public void delete(Integer id) {
			entityManager.remove(searchById(id));
		}

		@Override
		public void updatePortfolio(Portfolio portfolio) {
			entityManager.merge(portfolio);
			
		}

		@Override
		public void update(Portfolio portfolio) {
		 entityManager.merge(portfolio);
			
		}



		@Override
		public Portfolio searchById(Integer id) {
			return entityManager.find(Portfolio.class, id);
			
		}
		@Override
		public Portfolio searchById2(Integer id) {
			Portfolio p= entityManager.find(Portfolio.class, id);
			Portfolio p2=new Portfolio();
			p2.setType(p.getType());
			return p2;
		}
		
		@Override
		public List<Portfolio> getAllPortfolio(int id) {
			// TypedQuery<CurrencyOption> query = em.createQuery("Select c from
			// CurrenyOption c", CurrencyOption.class);
			//TypedQuery<Portfolio> query = entityManager.createQuery("Select c from " + Portfolio.class.getName() + " c ",
				//	Portfolio.class);

			//return query.getResultList();
			///select DISTINCT m from StockOption m join m.portfolio e where e.id=:portfolioid
			//return entityManager.createQuery("select DISTINCT m from Portfolio m join m.user e where e.id=:id",Portfolio.class).getResultList();
			TypedQuery<Portfolio> query=entityManager.createQuery("select DISTINCT m from Portfolio m join m.user e where e.id=:id",Portfolio.class);
			query.setParameter("id",id);
			return query.getResultList();
			
			
		}
		public List<Portfolio> getPortfolios(){
			CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
	        cq.select(cq.from(Portfolio.class));
	        return entityManager.createQuery(cq).getResultList();
		}



		



		@Override
		public void affecterOptionPortfolio(int PortfolioId, int OptionId) {
			Portfolio e=entityManager.find(Portfolio.class,PortfolioId);
			StockOption d=entityManager.find(StockOption.class, OptionId);
		
			d.setPortfolio(e);
			
		}
		@Override
		public void affecterOptionPortfolio1(int PortfolioId, int OptionId) {
			Portfolio e=entityManager.find(Portfolio.class,PortfolioId);
			CurrencyOption d=entityManager.find(CurrencyOption.class, OptionId);
		
			d.setPortfolio(e);
			
		}



		/*@Override
		public void addTrader(Trader trader) {
			entityManager.persist(trader);
		
			
		}
*/


		@Override
		public void affecterTraderPortfolio(int PortfolioId, int UserId) {
			Portfolio e=entityManager.find(Portfolio.class,PortfolioId);
			User d=entityManager.find(User.class, UserId);
		
			e.setUser(d);
			
		}



		@Override
		public void addOption(int portfolioid,int optionid) {
			Portfolio p=entityManager.find(Portfolio.class, portfolioid);
			StockOption s=entityManager.find(StockOption.class, optionid);
			s.setPortfolio(p);
			
			//entityManager.persist(option);
		}
		@Override
		public void addOption1(int portfolioid,int optionid) {
			Portfolio p=entityManager.find(Portfolio.class, portfolioid);
            CurrencyOption s=entityManager.find(CurrencyOption.class, optionid);
			s.setPortfolio(p);
			
			//entityManager.persist(option);
		}
		
		@Override
		public Long calculerStockOption(StockOption st)
		{
			String jpql = "SELECT count(*) from StockOption " ;
			Query query = entityManager
					.createQuery(jpql);
			List<Long> res = query.getResultList();
			if ((res != null) && (!res.isEmpty())) {
				return res.get(0);
			}
			return null;
		}

		@Override
		public Long calculerCurrencyOption(CurrencyOption cu)
		{String jpql = "SELECT count(*) from CurrencyOption " ;
		Query query = entityManager
				.createQuery(jpql);
		List<Long> res1 = query.getResultList();
		if ((res1 != null) && (!res1.isEmpty())) {
			return res1.get(0);
		}
		return null;
			
		}
		@Override
		public Long calsom(CurrencyOption cu,StockOption st)
		{
		
			return calculerCurrencyOption(cu)+calculerStockOption(st);
		}



		@Override
		public List<StockOption> getAllOption(int portfolioid) {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e where e.id=:portfolioid",StockOption.class);
			query.setParameter("portfolioid",portfolioid);
			return query.getResultList();
			
		}
		@Override
		public List<StockOption> getAllOptionentry() {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("from StockOption m where m.potfolio=null",StockOption.class);
			
			return query.getResultList();
			
		}
		@Override
		public List<StockOption> getAllOptionuser(int id) {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e join e.user u where u.id=:id",StockOption.class);
			query.setParameter("id",id);
			return query.getResultList();
			
		}
		@Override
		public List<StockOption> getAllOptiontobuy(int id) {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e join e.user u where u.id!=:id",StockOption.class);
			query.setParameter("id",id);
			return query.getResultList();
			
		}
		@Override
		public List<StockOption> getAllOptiontosell(int id) {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e join e.user u where u.id=:id",StockOption.class);
			query.setParameter("id",id);
			return query.getResultList();
			
		}
		
		
		@Override
		public List<StockOption> getAllOption1(int id) {
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT s from StockOption s join s.user u where u.id=:id)",StockOption.class);
			//TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e where e.id=:portfolioid",StockOption.class);
			//query.setParameter("portfolioid",portfolioid);
			query.setParameter("id",id);
			return query.getResultList();
			
		}
		@Override
		public void desaffecterOptionPortfolio(int PortfolioId, int OptionId) {
			//Portfolio e=entityManager.find(Portfolio.class,PortfolioId);
			StockOption d=entityManager.find(StockOption.class, OptionId);
		
			d.setPortfolio(null);
			
		
		
		
		
		
		}
		
		@Override
		public List<CurrencyOption> getAllOption2(int portfolioid) {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<CurrencyOption> query=entityManager.createQuery("select DISTINCT m from CurrencyOption m join m.portfolio e where e.id=:portfolioid",CurrencyOption.class);
			query.setParameter("portfolioid",portfolioid);
			return query.getResultList();
			
		}
		
		@Override
		public List<CurrencyOption> getAllOption22() {
			return entityManager.createQuery("from CurrencyOption",CurrencyOption.class).getResultList();
			//TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption m join m.portfolio e where e.id=:portfolioid",StockOption.class);
			//query.setParameter("portfolioid",portfolioid);
			//return query.getResultList();
			
		}
		@Override
		public void desaffecterOption1Portfolio(int PortfolioId, int OptionId) {
			//Portfolio e=entityManager.find(Portfolio.class,PortfolioId);
			CurrencyOption d=entityManager.find(CurrencyOption.class, OptionId);
		
			d.setPortfolio(null);
			
		
		
		
		
		
		}
		@Override
		public List<StockOption> getAllOptionmarket() {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<StockOption> query=entityManager.createQuery("select DISTINCT m from StockOption  where m.status=:1",StockOption.class);
			
			return query.getResultList();
			
		}
		@Override
		public void changerstoptionstatus(StockOption st)
		{
			entityManager.merge(st);
		}
		
		@Override
		public void achatoption(int idtrader, int idoption,  String dateSettelment)
		{
			//entityManager.persist(idtrader,idoption,dateSettelment);
			Trade e=entityManager.find(Trade.class,idoption);
			User d=entityManager.find(User.class, idtrader);
			//StockOption s=entityManager.find(StockOption.class,idoption);
			//Portfolio p=entityManager.find(Portfolio.class,s.getPortfolio().getId());
			
			
		
		
			
			e.setCounterparty(d.getFirst_name());
			//s.setPortfolio(null);
			//s.setUser(d);
			
		}
		
		@Override
		public void actualiserprix(float prix,int id)
		{
			User d=entityManager.find(User.class, id);
			d.setAmount(d.getAmount()-prix);
			
		}
		@Override
		public void selloption(int idtrader, int idoption,  String dateSettelment)
		{
			//entityManager.persist(idtrader,idoption,dateSettelment);
			StockOption e=entityManager.find(StockOption.class,idoption);
			User d=entityManager.find(User.class, idtrader);
			Portfolio p=entityManager.find(Portfolio.class,e.getPortfolio().getId());
			
		
		
			
			
			e.setPortfolio(null);
			e.setUser(d);
			
		}
		
		@Override
		public void actualiserprix1(float prix,int id)
		{
			User d=entityManager.find(User.class, id);
			d.setAmount(d.getAmount()+prix);
			
		}
		@Override
		public void actualiserstatus(int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setStatus(2);
			
		}
		@Override
		public void actualiserstatus1(int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setStatus(0);
			
		}
		@Override
		public void actualiserstatus2(int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setStatus(5);
			
		}
		@Override
		public void actualiserstatus3(int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setStatus(1);
			
		}
		@Override
		public void ajoutertrade(Trade trade)
		{
			
			entityManager.persist(trade);
		}
		
		@Override
		public List<Trade> getAllTradetobuy() {
			//return entityManager.createQuery("from StockOption",StockOption.class).getResultList();
			TypedQuery<Trade> query=entityManager.createQuery("from Trade",Trade.class);
			
			return query.getResultList();
			
		}
		
		@Override
		public void actualiserbuyer(int id1,int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setBuyer(id1);
			
		}
		@Override
		public void actualisercounterparty(String id1,int id)
		{
			Trade d=entityManager.find(Trade.class, id);
			d.setCounterparty(id1);
			
		}
		@Override
		public void actualiserOption(int id,int idtrader)
		{
			
			StockOption d=entityManager.find(StockOption.class, id);
			User u=entityManager.find(User.class, idtrader);
			//Portfolio p=entityManager.find(Portfolio.class,d.getPortfolio().getId());
			d.setPortfolio(null);
			d.setUser(u);
			
		}
}





	
