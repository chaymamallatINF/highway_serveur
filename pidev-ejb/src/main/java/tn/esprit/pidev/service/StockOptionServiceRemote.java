package tn.esprit.pidev.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.entity.StockOption;
import tn.esprit.pidev.entity.Portfolio;
//import tn.esprit.pidev.entity.StockOption;
//import tn.esprit.pidev.entity.User;



@Remote
public interface StockOptionServiceRemote {
	
	
	public  int ajouterStockOption(StockOption stockoption);
	public void deleteStockOptionByid(int StockOptionId);
	public StockOption getStockOptionById(int StockOptionId);
	public void UpdateStockOption(int StockOptionId,float Strike,int volume,int open_interest,int nbr_shares);
	public List<StockOption> getStockOptions(); 
	

}
