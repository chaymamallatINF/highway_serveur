package tn.esprit.pidev.service;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.entity.CurrencyOption;
import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.entity.StockOption;
//import tn.esprit.pidev.entity.Trader;
import tn.esprit.pidev.entity.Trade;

@Remote
public interface PortfolioInterface {
void addPortfolio(Portfolio portfolio);
void addOption(int portfolioid,int optionid);
void addOption1(int portfolioid,int optionid);
//void addTrader(Trader trader);
public List<StockOption> getAllOptionentry();
void affecterOptionPortfolio(int PortfolioId,int OptionId);
void affecterOptionPortfolio1(int PortfolioId,int OptionId);
public void desaffecterOptionPortfolio(int PortfolioId, int OptionId);
void affecterTraderPortfolio(int PortfolioId,int UserId);
Portfolio searchById(Integer id);
Portfolio searchById2(Integer id);
void delete(Integer id);
void updatePortfolio(Portfolio portfolio);
void update(Portfolio portfolio);
List<Portfolio> getAllPortfolio(int id);
List<StockOption> getAllOption(int portfolioid);
public List<StockOption> getAllOption1(int id);
public Long calculerStockOption(StockOption st);
public Long calculerCurrencyOption(CurrencyOption cu);
public Long calsom(CurrencyOption cu,StockOption st);
public List<CurrencyOption> getAllOption2(int portfolioid);
public List<CurrencyOption> getAllOption22();
public void desaffecterOption1Portfolio(int PortfolioId, int OptionId);
public List<StockOption> getAllOptionmarket();
public void changerstoptionstatus(StockOption st);
public List<StockOption> getAllOptionuser(int id);
public List<StockOption> getAllOptiontobuy(int id);
public void achatoption(int idtrader, int idoption,  String dateSettelment);
public void actualiserprix(float prix,int id);
public List<StockOption> getAllOptiontosell(int id);
public void actualiserprix1(float prix,int id);
public void selloption(int idtrader, int idoption,  String dateSettelment);
public void actualiserstatus(int id);
public void actualiserstatus1(int id);
public void ajoutertrade(Trade trade);
public List<Trade> getAllTradetobuy();
public void actualiserbuyer(int id1,int id);
public void actualiserstatus2(int id);
public void actualiserstatus3(int id);
public void actualisercounterparty(String id1,int id);
public void actualiserOption(int id,int idtrader);
}
