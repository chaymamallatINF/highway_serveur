package tn.esprit.pidev.service;




import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pidev.entity.StockOption;
import tn.esprit.pidev.entity.Stock ;

@Stateless
public class StockService implements StockRemoteService {
	
	
	@PersistenceContext(unitName="pidev-ejb")
	EntityManager opst; // synchro BD reqqq
//Houni ena n7eb n assuri el cnx avec la BD 
	// bch nest3amel les fcts predefinis mte3 el Hyper
	// bch ne5dem 7keyet el metier w n assuri l ajout wala l fct fel BD 
	// men Couche l wa7da o5ra !! 
	
	
	@Override
	public int ajouterStock(Stock stock) {
		// TODO Auto-generated method stub
		opst.persist(stock);
		return stock.getId();
		//return 1;
	}

	@Override
	public void deleteStockByid(int StockId) {
		// TODO Auto-generated method stub
		
		opst.remove(opst.find(Stock.class, StockId));
		
	}

	@Override
	public Stock getStockById(int StockId) {
		// TODO Auto-generated method stub
	
		TypedQuery<Stock> query=opst.createQuery("SELECT o FROM Stock o WHERE o.id=:StockId", Stock.class);
		query.setParameter("StockId",StockId);
	
		return query.getSingleResult();
		
	}

	@Override
	public void UpdateStock(int StockId, int nbr_total_of_shares, float total_value) {
		// TODO Auto-generated method stub
		
		Stock stock=opst.find(Stock.class, StockId);
		stock.setNbr_total_of_shares(nbr_total_of_shares);
		stock.setTotal_value(total_value);
		
	}

	@Override
	public List<Stock> getStock() {
		// TODO Auto-generated method stub
		
		
		TypedQuery<Stock> query=opst.createQuery("SELECT o FROM Stock o",Stock.class);
		return query.getResultList();// hhhhhh
		
	}
	
	
	
	

}
