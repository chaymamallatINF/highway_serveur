package tn.esprit.pidev.service;



import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pidev.entity.StockOption;
//import tn.esprit.pidev.entity.StockOption;
//Haka dhorli !! 
import tn.esprit.pidev.entity.User;

@Stateless

public class StockOptionService implements StockOptionServiceRemote {

	@PersistenceContext(unitName="pidev-ejb")
	EntityManager op; // synchro BD reqqq
	
	@Override
	public int ajouterStockOption(StockOption stockoption) {
		// TODO Auto-generated method stub
		op.persist(stockoption);
		return stockoption.getId();
		
		
	}

	@Override
	public void deleteStockOptionByid(int StockOptionId) {
		// TODO Auto-generated method stub
		
		op.remove(op.find(StockOption.class, StockOptionId));
		
	}

	@Override
	public StockOption getStockOptionById(int StockOptionId) {
		// TODO Auto-generated method stub
		
		TypedQuery<StockOption> query=op.createQuery("SELECT o FROM StockOption o WHERE o.id=:StockOptionId", StockOption.class);
		query.setParameter("StockOptionId",StockOptionId);
	
		return query.getSingleResult();//traja3 resultat barka
	
		
	}

	@Override
	public void UpdateStockOption(int StockOptionId, float Strike, int volume, int open_interst,int nbr_shares) {
		// TODO Auto-generated method stub
		StockOption stockoption=op.find(StockOption.class, StockOptionId);
		stockoption.setStrike(Strike);
		stockoption.setVolume(volume);
		stockoption.setOpen_interst(open_interst);
		stockoption.setNbr_shares(nbr_shares);
		
		
	}

	@Override
	public List<StockOption> getStockOptions() {
		// TODO Auto-generated method stub
		//return null;
		
		TypedQuery<StockOption> query=op.createQuery("SELECT o FROM StockOption o",StockOption.class);
		return query.getResultList();// ema hedhi traja3 lista !!
	}

	
	
}
