package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class CurrencyOption implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String contract_name;
	private Date last_trade_date;
	private float strike;
	private float last_price;
	private float bid;
	private float ask;
	private float change_value;
	private int volume;
	private int open_interst;
	private float implied_volatility;
	private String devise;
	@ManyToOne
	private Portfolio portfolio;
	public Portfolio getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContract_name() {
		return contract_name;
	}
	public void setContract_name(String contract_name) {
		this.contract_name = contract_name;
	}
	public Date getLast_trade_date() {
		return last_trade_date;
	}
	public void setLast_trade_date(Date last_trade_date) {
		this.last_trade_date = last_trade_date;
	}
	public float getStrike() {
		return strike;
	}
	public void setStrike(float strike) {
		this.strike = strike;
	}
	public float getLast_price() {
		return last_price;
	}
	public void setLast_price(float last_price) {
		this.last_price = last_price;
	}
	public float getBid() {
		return bid;
	}
	public void setBid(float bid) {
		this.bid = bid;
	}
	public float getAsk() {
		return ask;
	}
	public void setAsk(float ask) {
		this.ask = ask;
	}
	public float getChange_value() {
		return change_value;
	}
	public void setChange_value(float change_value) {
		this.change_value = change_value;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public int getOpen_interst() {
		return open_interst;
	}
	public void setOpen_interst(int open_interst) {
		this.open_interst = open_interst;
	}
	public float getImplied_volatility() {
		return implied_volatility;
	}
	public void setImplied_volatility(float implied_volatility) {
		this.implied_volatility = implied_volatility;
	}
	public String getDevise() {
		return devise;
	}
	public void setDevise(String devise) {
		this.devise = devise;
	}
	

}
