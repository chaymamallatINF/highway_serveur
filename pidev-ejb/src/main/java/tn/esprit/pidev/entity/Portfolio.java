package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;




@Entity
public class Portfolio implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String type;
	private String category;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@OneToMany(mappedBy="portfolio",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
	private List<CurrencyOption> currency_option;
	@OneToMany(mappedBy="portfolio",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
	private List<StockOption> stock_option;

	@ManyToOne
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String toString() {
		return "Portfolio [id=" + id + ", type=" + type + ",categry="+category+ ", trader="  + "]";
	}
	public List<CurrencyOption> getCurrency_option() {
		return currency_option;
	}
	public void setCurrency_option(List<CurrencyOption> currency_option) {
		this.currency_option = currency_option;
	}
	public List<StockOption> getStock_option() {
		return stock_option;
	}
	public void setStock_option(List<StockOption> stock_option) {
		this.stock_option = stock_option;
	}
	/*public String getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(String type) {
		this.type = type;
	}*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Portfolio other = (Portfolio) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	public Portfolio(String type, String category) {
		super();
		this.type = type;
		this.category = category;
	}
	public Portfolio() {
		super();
	}

	public Portfolio(String type, String category,
			Integer employeIdToBeUpdated) {
		this.type = type;
		this.category = category;
		
		this.id = employeIdToBeUpdated;
	}

}
