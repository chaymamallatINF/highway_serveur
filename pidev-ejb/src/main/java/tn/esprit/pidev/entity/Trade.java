package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Trade implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String contract_name;
	private String counterparty;
	private Date settlement_date;
	private float price;
	private float strike;
	private float bid;
	private float ask;
	private int volume;
	private int status;
	private int buyer;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContract_name() {
		return contract_name;
	}
	public void setContract_name(String contract_name) {
		this.contract_name = contract_name;
	}
	public String getCounterparty() {
		return counterparty;
	}
	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}
	public Date getSettlement_date() {
		return settlement_date;
	}
	public void setSettlement_date(Date settlement_date) {
		this.settlement_date = settlement_date;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getStrike() {
		return strike;
	}
	public void setStrike(float strike) {
		this.strike = strike;
	}
	public float getBid() {
		return bid;
	}
	public void setBid(float bid) {
		this.bid = bid;
	}
	public float getAsk() {
		return ask;
	}
	public void setAsk(float ask) {
		this.ask = ask;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	@Override
	public String toString() {
		return "Trade [id=" + id + ", contract_name=" + contract_name + ", counterparty=" + counterparty
				+ ", settlement_date=" + settlement_date + ", price=" + price + ", strike=" + strike + ", bid=" + bid
				+ ", ask=" + ask + ", volume=" + volume + "]";
	}
	/*public Trade(String contract_name, String counterparty, float price) {
		super();
		this.contract_name = contract_name;
		this.counterparty = counterparty;
		this.price = price;
	}*/
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((counterparty == null) ? 0 : counterparty.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trade other = (Trade) obj;
		if (counterparty == null) {
			if (other.counterparty != null)
				return false;
		} else if (!counterparty.equals(other.counterparty))
			return false;
		return true;
	}
	public int getBuyer() {
		return buyer;
	}
	public void setBuyer(int buyer) {
		this.buyer = buyer;
	}
	
	

}
