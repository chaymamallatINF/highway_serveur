package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Stock implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int nbr_total_of_shares;
	private float total_value;
	@OneToMany
	private List<StockOption> stock_option;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNbr_total_of_shares() {
		return nbr_total_of_shares;
	}
	public void setNbr_total_of_shares(int nbr_total_of_shares) {
		this.nbr_total_of_shares = nbr_total_of_shares;
	}
	public float getTotal_value() {
		return total_value;
	}
	public void setTotal_value(float total_value) {
		this.total_value = total_value;
	}
	public List<StockOption> getStock_option() {
		return stock_option;
	}
	public void setStock_option(List<StockOption> stock_option) {
		this.stock_option = stock_option;
	}
		

}
