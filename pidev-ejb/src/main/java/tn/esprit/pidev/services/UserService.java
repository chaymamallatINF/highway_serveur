package tn.esprit.pidev.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.entity.Type;

@Stateless
public  class UserService implements UserServiceRemote{

	@PersistenceContext(unitName="pidev-ejb")
	EntityManager em;
	public int ajouterUser(User user) {
	
		
		
		em.persist(user);
		return user.getId();
	}

	@Override
	public void deleteUserByid(int UserId) {
		em.remove(em.find(User.class, UserId));
		
	}
	
	@Override
	public User getUserById(int userId) {
	TypedQuery<User> query=em.createQuery("SELECT u FROM User u WHERE u.id=:userId", User.class);
			query.setParameter("userId",userId);
		return query.getSingleResult();
	}
	@Override
	public List<User> getUsers() {
		TypedQuery<User> query=em.createQuery("SELECT u FROM User u",User.class);
	return query.getResultList();
	}

	@Override
	public void UpdateUser(String adress,String password,String lastName,String tel,String firstName,Date dateOfBirth,String email,int UserId){
		User user=em.find(User.class, UserId);
		user.setAddress(adress);
		user.setPwd(password);
		user.setLast_name(lastName);
		user.setTel(tel);
		user.setFirst_name(firstName);
		user.setDate_of_birth(dateOfBirth);
		user.setMail(email);
	}
		@Override
		public User getUserByLoginPwd(String login, String pwd) {
			try {
				return em.createQuery("SELECT u FROM User u where u.mail= :mail  AND u.pwd = :pwd", User.class).setParameter("mail", login).setParameter("pwd", pwd).getSingleResult();
			} catch (NoResultException nr) {
				return null;
			}
		
		}

		@Override
		public boolean isExist(String email) {
			try{
			User u= em.createQuery("SELECT u FROM User u where u.mail= :mail", User.class)
					.setParameter("mail", email).getSingleResult();
			}
			catch (NoResultException nr)
			{
				return false;
				
			}
			return true;
			
		}

		@Override
		public List<User> getAdmin() {
			return em.createQuery("SELECT u from User u where u.type= :type ",User.class).setParameter("type",Type.Admin).getResultList();
 
		
		}

		@Override
		public List<User> getTrader() {
			
			return em.createQuery("SELECT u from User u where u.type= :type ",User.class).setParameter("type",Type.Trader).getResultList();

		}

		@Override
		public List<User> rech(String rech) {
		
			return em.createQuery("SELECT u from User u where u.first_name LIKE :rech OR u.last_name LIKE :rech ",User.class).setParameter("rech","%"+rech+"%").getResultList();

		}

		@Override
		public void blockUser(int userId) {
			User user=em.find(User.class, userId);
			if(user.isActive())
			user.setActive(false);
			else
				user.setActive(true);
		}
		
	

	


	
}
