package tn.esprit.pidev.services;

import java.util.List;

import javax.ejb.Remote;


import tn.esprit.pidev.entity.Reclamation;

@Remote
public interface ReclamationServiceRemote {
	public int ajoutReclamation(Reclamation c);
	public void deleteReclamationById(int ClaimId);
	public void updateReclamation(String type, String content,String option, String response, int Claim_id);
	public List<Reclamation> getReclamation();
	public List<Reclamation> rech(String rech);
	public Reclamation getReclamationById(int ClaimId);
	public void responseReclamation(int ClaimId,String response);

}
