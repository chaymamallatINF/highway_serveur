package tn.esprit.pidev.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.entity.User;
@Remote
public interface UserServiceRemote {
	public  int ajouterUser(User user);
	public void deleteUserByid(int UserId);
	public User getUserById(int userId);
	public void UpdateUser(String adress,String password,String lastName,String tel,String firstName,Date dateOfBirth,String email,int UserId);
	public List<User> getUsers();
	public User getUserByLoginPwd(String login, String pwd);
	public boolean isExist(String email) ;
	List<User> getAdmin();
	List<User> getTrader();
	public List<User> rech(String rech);
	public void blockUser(int userId);
	

}
