package tn.esprit.pidev.services;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import tn.esprit.pidev.entity.Reclamation;
import tn.esprit.pidev.entity.User;

@Stateless
public class ReclamationService  implements ReclamationServiceRemote{
	@PersistenceContext(unitName="pidev-ejb")
	EntityManager em;


	@Override
	public void deleteReclamationById(int ClaimId) {
	em.remove(em.find(Reclamation.class,ClaimId));
		
	}


	@Override
	public List<Reclamation> getReclamation() {
		TypedQuery<Reclamation> query=em.createQuery("SELECT c FROM Reclamation c",Reclamation.class);
		return query.getResultList();
	
	}

	@Override
	public List<Reclamation> rech(String rech) {
		return em.createQuery("SELECT c from Reclamation c where c.status LIKE :rech",Reclamation.class).setParameter("rech","%"+rech+"%").getResultList();

	}

	@Override
	public Reclamation getReclamationById(int ClaimId) {
		TypedQuery<Reclamation> query=em.createQuery("SELECT c FROM Reclamation c WHERE c.id=:ClaimId", Reclamation.class);
		query.setParameter("ClaimId",ClaimId);
	return query.getSingleResult();
	}

	@Override
	public void updateReclamation(String type, String content,String option, String response, int Claim_id) {
		Reclamation c= em.find(Reclamation.class,Claim_id );
 c.setType(type);
 c.setContent(content);
 c.setOption(option);
 c.setResponse(response);
 
		
	}

	@Override
	public void responseReclamation(int ClaimId, String response) {
		Reclamation c =em.find(Reclamation.class,ClaimId);
		c.setResponse(response);
		c.setStatus("treated");
		
	}

	@Override
	public int ajoutReclamation(Reclamation c) {
		em.persist(c);
		return c.getId();
	}

	
}
