package tn.esprit.pidev.presentation.mbeans;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.pidev.entity.Reclamation;
import tn.esprit.pidev.services.ReclamationService;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

@ManagedBean
@SessionScoped
public class ClaimsBean {
private String content;
private String response;
private String Status;
private Date claimDate;
private String type;
private String option;
private List<Reclamation> claims;
private Reclamation claim;
private Integer claimIdtoBeRespond;

private List<HistoricalQuote>shares;
private HistoricalQuote share=new HistoricalQuote();
private String symbole;


@EJB
ReclamationService claimService;
@PostConstruct
public void init(){
	claimDate=new Date();
	Status="not treated";
	response=" ";

	
}
public void searchforStocksbysymbol(String period, int num) {
	Stock stock = null;
	if (period.equals("none") && num == 0) {
		try {
			
			stock = YahooFinance.get(symbole, true);
			
		} catch (IOException ex) {

			System.out.println("gs");
		}
	} else {
		try {
			Calendar from = Calendar.getInstance();
			Calendar to = Calendar.getInstance();
			if (period.equals("Month")) {
				from.add(Calendar.MONTH, -num);
				stock = YahooFinance.get(symbole, from, to, Interval.MONTHLY);
			} else if (period.equals("Year")) {
				from.add(Calendar.YEAR, -num);
				stock = YahooFinance.get(symbole, from, to, Interval.WEEKLY);
			} else if (period.equals("Day")) {
				from.add(Calendar.DAY_OF_YEAR, -num);
				stock = YahooFinance.get(symbole, from, to, Interval.DAILY);
			}

		} catch (IOException ex) {
		
			System.out.println("hh");
		}
	}

	shares = new ArrayList<HistoricalQuote>();
	try {
		shares = stock.getHistory();

	} catch (IOException e) {

		e.printStackTrace();
	}
	System.out.println("hhdjdjdjjd");
	for (HistoricalQuote historicalQuote : shares) {
		System.out.println("symbol " + historicalQuote.getSymbol());
		System.out.println("adj close" + historicalQuote.getAdjClose());
		System.out.println("close " + historicalQuote.getClose());
		System.out.println("date " + historicalQuote.getDate().getTime());
		System.out.println("high" + historicalQuote.getHigh());
		System.out.println("low " + historicalQuote.getLow());
		System.out.println("open " + historicalQuote.getOpen());
		System.out.println("volume " + historicalQuote.getVolume());

	}

}

public void getstocks(){
	searchforStocksbysymbol("none", 0);
	FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("formyahoo:dt");
	
}
public void addClaim(){
	
	SimpleDateFormat Format=new SimpleDateFormat();
	System.out.println("date"+ Format.format(claimDate));
	Reclamation claim=new Reclamation(option,content, claimDate, Status);
	claimService.ajoutReclamation(claim);
	
}
public void Respond(Reclamation claim){
this.setContent(claim.getContent());
this.setClaimIdtoBeRespond(claim.getId());
	
}
public void repond1(){
	claimService.updateReclamation(new Reclamation(claimIdtoBeRespond, content, claimDate, response, "treated", option));
}
public Reclamation rechercheParId(int id) {
	return claimService.getReclamationById(id);

}

public List<Reclamation> getAllClaims(){
	claims=claimService.getReclamation();
	return claims;
}
public void deleteClaim(Integer claimId){
	claimService.deleteReclamationById(claimId); 
	
}

public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public String getResponse() {
	return response;
}
public void setResponse(String response) {
	this.response = response;
}
public String getStatus() {
	return Status;
}
public void setStatus(String status) {
	Status = status;
}
public Date getClaimDate() {
	return claimDate;
}
public void setClaimDate(Date claimDate) {
	this.claimDate = claimDate;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getOption() {
	return option;
}
public void setOption(String option) {
	this.option = option;
}
public List<Reclamation> getClaims() {
	return claims;
}
public void setClaims(List<Reclamation> claims) {
	this.claims = claims;
}
public Reclamation getClaim() {
	return claim;
}
public void setClaim(Reclamation claim) {
	this.claim = claim;
}
public Integer getClaimIdtoBeRespond() {
	return claimIdtoBeRespond;
}
public void setClaimIdtoBeRespond(Integer claimIdtoBeRespond) {
	this.claimIdtoBeRespond = claimIdtoBeRespond;
}


public void SearchByType(String status){
	claims.addAll(claimService.rech(status));
}
public List<HistoricalQuote> getShares() {
	return shares;
}
public void setShares(List<HistoricalQuote> shares) {
	this.shares = shares;
}

public String getSymbole() {
	return symbole;
}
public void setSymbole(String symbole) {
	this.symbole = symbole;
}
public void getValues(HistoricalQuote stock){
stock.setLow(stock.getLow());
stock.setHigh(stock.getHigh());
stock.setClose(stock.getClose());
stock.setOpen(stock.getOpen());
System.out.println("ttttttttttt");
}
public String calculeAverage(){
	HistoricalQuote stock=new HistoricalQuote();
	String average="";
	Float a=Float.parseFloat(stock.getHigh().toString());
	Float b=Float.parseFloat(stock.getClose().toString());
	Float c=Float.parseFloat(stock.getLow().toString());
	Float d=Float.parseFloat(stock.getOpen().toString());
	Float e=(a*b*c*d)/4;
 return average=e.toString();
	
}
/**
 * @return the share
 */
public HistoricalQuote getShare() {
	return share;
}
/**
 * @param share the share to set
 */
public void setShare(HistoricalQuote share) {
	this.share = share;
}



}
