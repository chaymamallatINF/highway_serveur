package tn.esprit.pidev.presentation.mbeans;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import tn.esprit.pidev.presentation.mbeans.PortfolioBean;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.entity.StockOption;
import tn.esprit.pidev.service.PortfolioService;
import tn.esprit.pidev.service.StockOptionService;



@ManagedBean
@SessionScoped
public class OptionBean {
	
	private int id;
	private String contact_name;
	private float ask;
	private float bid;
	private float strike;
	private float volume;
	
	private List<StockOption> options;
	private List<StockOption> options1;
	private List<Portfolio> portfolios;
	private Integer optionIdToBeUpdated;
	private Integer selectedOptionId;
	public List<StockOption> getOptions1() {
		return options1;
	}

	public void setOptions1(List<StockOption> options1) {
		this.options1 = options1;
	}

	public List<Portfolio> getPortfolios() {
		return portfolios;
	}

	public void setPortfolios(List<Portfolio> portfolios) {
		this.portfolios = portfolios;
	}

	public Integer getSelectedOptionId() {
		return selectedOptionId;
	}

	public void setSelectedOptionId(Integer selectedOptionId) {
		this.selectedOptionId = selectedOptionId;
	}
	@EJB
	PortfolioService optionService;
	
	public void init() {
		
		//options = optionService.getAllOption1(2);
	}
	
	public void addOption() {
		//System.out.println(portfolioService.getPortfolios());
		StockOption selectedEmploye = new StockOption();
		//selectedEmploye.setId(selectedOptionId);
		System.out.println(PortfolioBean.x.getId()+"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
		//selectedEmploye.setPortfolio(PortfolioBean.x);
		//optionService.affecterOptionPortfolio(2,11);
		
		//.setEmploye(selectedEmploye);
		//int i = employeService.ajouterContrat(contrat);
		//employeService.affecterContratAEmploye(i, selectedEmployeId);
	}
	
	public void supprimer(StockOption o) {
		optionService.desaffecterOptionPortfolio(PortfolioBean.x.getId(), o.getId());
	}
	
	/*public void modifier(Portfolio employe) {
		this.setCategory(employe.getCategory());
		this.setType(employe.getType());
		this.setPortfolioIdToBeUpdated(employe.getId());
	}
	
	public void mettreAjourEmploye() {
		System.out.println(portfolioIdToBeUpdated);
		portfolioService.update(new Portfolio(type,category,portfolioIdToBeUpdated));
	}
	public String passage(Portfolio x){
		String navigateTo = "null";
		
		//employe = employeService.getEmployeByEmailAndPassword(login, password);((x.getType().equals("defensive"))||(x.getType().equals("aggressive")))
		if((x.getType().equals("defensive"))||(x.getType().equals("aggressive"))) {
			System.out.println("hello world");
			navigateTo="ContenuPortfolio?faces-redirect=true";
			
		}else {
			FacesContext.getCurrentInstance().addMessage("form:btn",new FacesMessage("bad credentials"));
		}
		return navigateTo;
	}*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public float getAsk() {
		return ask;
	}

	public void setAsk(float ask) {
		this.ask = ask;
	}

	public float getBid() {
		return bid;
	}

	public void setBid(float bid) {
		this.bid = bid;
	}

	public float getStrike() {
		return strike;
	}

	public void setStrike(float strike) {
		this.strike = strike;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public List<StockOption> getOptions() {
		return options;
	}

	public void setOptions(List<StockOption> options) {
		this.options = options;
	}

	public Integer getOptionIdToBeUpdated() {
		return optionIdToBeUpdated;
	}

	public void setOptionIdToBeUpdated(Integer optionIdToBeUpdated) {
		this.optionIdToBeUpdated = optionIdToBeUpdated;
	}

	

	public PortfolioService getOptionService() {
		return optionService;
	}

	public void setOptionService(PortfolioService optionService) {
		this.optionService = optionService;
	}

	public List<StockOption> getOs() {
		options=optionService.getAllOption(2);
		System.out.println(PortfolioBean.x.getId()+"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
		return options;
	}
	public List<StockOption> 	getCbox(){
	//ObservableList<StockOption> combojobs = FXCollections.observableArrayList();
    List<StockOption> jobs = optionService.getAllOption1(2); //getall //getall
    System.out.println(jobs);
   /* for (StockOption job : jobs) {
    	if((job.getPortfolio()==null)){
    		
        options1.add(job);
   
    }}*/
    System.out.println(PortfolioBean.x.getId()+"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
   return jobs;
    
} 

	
	
}
