package tn.esprit.pidev.presentation.mbeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.pidev.entity.Type;

@ManagedBean
@ApplicationScoped
public class Data {
	
	public Type[] getType(){
		
		return Type.values();
		}
}
