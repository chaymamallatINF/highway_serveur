package tn.esprit.pidev.presentation.mbeans;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import tn.esprit.pidev.entity.Type;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserService;

					
@ViewScoped
@ManagedBean
public class ProfileBean {
	
	@ManagedProperty(value ="#{loginBean}")
	private LoginBean loginBean;
	private String FirstName;
	private String lastName;
	private String address;
	private String mail;
	private Date date_of_birth;
	private String oldPwd;
	private String newPwd;
	private String newPwdC;
	private String tel;
	private Type type;
	private User user;
	private Integer UserIdToBeUpdated;
	@EJB
	UserService userService;

@PostConstruct
	private void init(){
		
		
	
		this.UserIdToBeUpdated=loginBean.getUser().getId();

		this.FirstName=loginBean.getUser().getFirst_name();
		this.lastName=loginBean.getUser().getLast_name();
		this.address=loginBean.getUser().getAddress();
		this.mail=loginBean.getUser().getMail();
		this.date_of_birth=loginBean.getUser().getDate_of_birth();
		this.tel=loginBean.getUser().getTel();
		//this.oldPwd=loginBean.getUser().getPwd();
		this.type=loginBean.getUser().getType();
		
		
		
	}
	
	public void modifier(){		
		userService.UpdateUser(new User(UserIdToBeUpdated,FirstName, lastName, tel, address, mail, loginBean.getUser().getPwd(), date_of_birth,type));
	 	
	}
	public void modiferPwd ()
	{
		User u = loginBean.getUser();
		if(u.getPwd().equals(oldPwd))
		{
			if(newPwd.equals(newPwdC))
			{
				u.setPwd(newPwd);
				userService.UpdateUser(u);
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage("ProfileFrom:newpwd2", new FacesMessage("Passwords Don't match"));
			}
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage("ProfileFrom:oldpwd", new FacesMessage("The old pass is incorrect")); 
		
		}
		
	}

	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getOldPwd() {
		return oldPwd;
	}

	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public String getNewPwdC() {
		return newPwdC;
	}

	public void setNewPwdC(String newPwdC) {
		this.newPwdC = newPwdC;
	}

	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}


	public Date getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getUserIdToBeUpdated() {
		return UserIdToBeUpdated;
	}

	public void setUserIdToBeUpdated(Integer userIdToBeUpdated) {
		UserIdToBeUpdated = userIdToBeUpdated;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	


}
