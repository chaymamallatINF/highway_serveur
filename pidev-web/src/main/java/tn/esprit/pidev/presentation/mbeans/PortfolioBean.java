package tn.esprit.pidev.presentation.mbeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


import tn.esprit.pidev.entity.Portfolio;
import tn.esprit.pidev.service.PortfolioService;



@ManagedBean
@SessionScoped
public class PortfolioBean {
	
	private int id;
	private String type;
	private String category;
	
	private List<Portfolio> portfolios;
	private Integer portfolioIdToBeUpdated;
	static  Portfolio x;
	@EJB
	PortfolioService portfolioService;
	
	public void addEmploye() {
		System.out.println(portfolioService.getPortfolios());
		portfolioService.addPortfolio(new Portfolio(type,category));
	}
	
	public void supprimer(Integer employeId) {
		portfolioService.delete(employeId);
	}
	
	public void modifier(Portfolio employe) {
		this.setCategory(employe.getCategory());
		this.setType(employe.getType());
		this.setPortfolioIdToBeUpdated(employe.getId());
	}
	
	public void mettreAjourEmploye() {
		System.out.println(portfolioIdToBeUpdated);
		portfolioService.update(new Portfolio(type,category,portfolioIdToBeUpdated));
	}
	public String passage(Portfolio x){
		String navigateTo = "null";
		
		//employe = employeService.getEmployeByEmailAndPassword(login, password);((x.getType().equals("defensive"))||(x.getType().equals("aggressive")))
		if((x.getType().equals("defensive"))||(x.getType().equals("aggressive"))) {
			System.out.println("hello world");
			navigateTo="ContenuPortfolio?faces-redirect=true";
			
		}else {
			FacesContext.getCurrentInstance().addMessage("form:btn",new FacesMessage("bad credentials"));
		}
		return navigateTo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Portfolio> getPortfolios() {
		return portfolios;
	}

	public void setPortfolios(List<Portfolio> portfolios) {
		this.portfolios = portfolios;
	}

	public Integer getPortfolioIdToBeUpdated() {
		return portfolioIdToBeUpdated;
	}

	public void setPortfolioIdToBeUpdated(Integer portfolioIdToBeUpdated) {
		this.portfolioIdToBeUpdated = portfolioIdToBeUpdated;
	}

	public PortfolioService getPortfolioService() {
		return portfolioService;
	}

	public void setPortfolioService(PortfolioService portfolioService) {
		this.portfolioService = portfolioService;
	}
	public List<Portfolio> getEmployes() {
		portfolios=portfolioService.getPortfolios();
		return portfolios;
	}

	
	
}
