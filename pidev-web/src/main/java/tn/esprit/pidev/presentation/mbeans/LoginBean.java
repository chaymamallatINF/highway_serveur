package tn.esprit.pidev.presentation.mbeans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.pidev.entity.Type;
import tn.esprit.pidev.entity.User;
import tn.esprit.pidev.services.UserService;

import org.primefaces.event.SelectEvent;

@SessionScoped
@ManagedBean
public class LoginBean {
	private String first_name;
	private String last_name;
	private String tel;
	private String address;
	private String mail;
	private String pwd;
	private boolean active=true;
	private User user;
	private boolean loggedIn;
	private Date date_of_birth;
	private List<User> users;
	private Integer UserIdToBeUpdated;
	private Type type;
	private String search;

	@EJB
	UserService userService;
	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
     
    

	
	public String doLogin(){
		String navigateTo="null";
		user=userService.getUserByLoginPwd(mail, pwd);
		if(user!=null && user.getType()==Type.Trader){
			navigateTo="/MyProfile?faces-redirect=true";
			loggedIn=true;
		}else if (user!=null && user.getType()==Type.Admin) {
			navigateTo = "/usersTable?faces-redirect=true"; 
			loggedIn = true;
		
		}else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials"));
		}
		return navigateTo;
		
	}
	public 	String doLogout(){
		 FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		 return"/template1?faces-recdirect=true";
		
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public Date getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	
	public Integer getUserIdToBeUpdated() {
		return UserIdToBeUpdated;
	}




	public void setUserIdToBeUpdated(Integer userIdToBeUpdated) {
		UserIdToBeUpdated = userIdToBeUpdated;
	}




	public void addUser(){
		SimpleDateFormat simpleFormat=new SimpleDateFormat("dd-MM-yyyy");
		System.out.println("date:"+ simpleFormat.format(date_of_birth));

		userService.ajouterUser(new User(first_name, last_name, tel, address, mail, pwd,date_of_birth));
	}




	public List<User> getUsers() {
		return users;
	}




	public String getSearch() {
		return search;
	}




	public void setSearch(String search) {
		this.search = search;
	}




	public void setUsers(List<User> users) {
		this.users = users;
	}
public List<User> getTraders(){
	users=userService.getUsers();
	return users;
}
public void supprimer(Integer UserId){
	userService.deleteUserByid(UserId);
}
public void modifier(User user){
	this.setFirst_name(user.getFirst_name());
	this.setLast_name(user.getLast_name());
	this.setDate_of_birth(user.getDate_of_birth());
	this.setMail(user.getMail());
	this.setPwd(user.getPwd());
	this.setTel(user.getTel());
	this.setAddress(user.getAddress());
	this.setUserIdToBeUpdated(user.getId());
	
}
public void mettreAjourUser(){
userService.UpdateUser(new User(UserIdToBeUpdated, first_name, last_name, tel, address, mail, pwd, date_of_birth,type));
}




public Type getType() {
	return type;
}




public void setType(Type type) {
	this.type = type;
}
public void handleKeyEvenet(){
	users.clear();
	users=new ArrayList<>();
	if(search==null || search.equals("")){
		search="";
		users=userService.getUsers();
	}else
			users=userService.rech(search);
		
		
	}
	

public void baneUser(int id){
	userService.blockUser(id);
}
}
