package validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("validator.PasswordValidator")

public class PasswordValidator implements Validator {
	private static final String PASS_PATTERN_MAJ="^.*[A-Z]+.*$";
	private static final String PASS_PATTERN_MIN="^.*[a-z]+.*$";
	private static final String PASS_PATTERN_NUM="^.*[0-9]+.*$";
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Pattern pattern_maj = Pattern.compile(PASS_PATTERN_MAJ);
		Pattern pattern_min= Pattern.compile(PASS_PATTERN_MIN);
		Pattern pattern_num= Pattern.compile(PASS_PATTERN_NUM);
		Matcher match=pattern_maj.matcher(value.toString());
		if(!match.matches())
		{
			FacesMessage msg = new FacesMessage("Le mot de passe doit au moins contenir une maj");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		match=pattern_min.matcher(value.toString());
		if(!match.matches())
		{
			FacesMessage msg = new FacesMessage("Le mot de passe doit au moins contenir une mini");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		match=pattern_num.matcher(value.toString());
		if(!match.matches())
		{
			FacesMessage msg = new FacesMessage("Le mot de passe doit au moins contenir un chiffre");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
		
	}


